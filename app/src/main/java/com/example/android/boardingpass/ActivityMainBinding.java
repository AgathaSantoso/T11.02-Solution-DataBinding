package com.example.android.boardingpass;

/**
 * Created by NAT on 21/03/2018.
 */

class ActivityMainBinding {
    public BreakIterator textViewPassengerName;
    public BreakIterator textViewOriginAirport;
    public BreakIterator textViewFlightCode;
    public BreakIterator textViewDestinationAirport;
    public BreakIterator textViewBoardingTime;
    public BreakIterator textViewDepartureTime;
    public BreakIterator textViewArrivalTime;
    public BreakIterator textViewBoardingInCountdown;
    public BreakIterator textViewTerminal;
    public BreakIterator textViewGate;
    public BreakIterator textViewSeat;
}
